package com.example.downloadmanager.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.downloadmanager.R
import com.example.downloadmanager.services.DownloadService
import com.example.downloadmanager.ui.widgets.DownloadTaskView

class DownloadTaskAdapter(
        private val tasks: List<DownloadService.DownloadTask>
) : RecyclerView.Adapter<DownloadTaskAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.item_download_task, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = tasks[position]
        holder.downloadTaskView.setTask(task)
    }

    override fun getItemCount(): Int = tasks.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal val downloadTaskView = view.findViewById<DownloadTaskView>(R.id.task)
    }

}