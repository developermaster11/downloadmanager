package com.example.downloadmanager.ui.activities

import android.Manifest
import android.content.*
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.downloadmanager.services.DownloadService
import com.example.downloadmanager.services.DownloadService.LocalBinder
import android.os.IBinder
import android.support.v4.content.LocalBroadcastManager
import com.example.downloadmanager.R
import com.example.downloadmanager.ui.adapters.DownloadTaskAdapter
import com.karumi.dexter.Dexter
import kotlinx.android.synthetic.main.activity_main.*
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener


class MainActivity : AppCompatActivity() {

    companion object {
        private const val PLACEHOLDER_URL = "https://get.videolan.org/vlc/3.0.0/win32/vlc-3.0.0-win32.exe" // i'm so sorry vlc...
    }

    private var service: DownloadService? = null

    /**
     * `true` if user had granted required runtime
     * permissions to user: WRITE/READ EXTERNAL STORAGE
     */
    private var permissionsGranted = false

    private val tasks: MutableList<DownloadService.DownloadTask> = ArrayList()

    /**
     * Defines callbacks for service binding,
     * passed to `bindService()`
     */
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, b: IBinder) {
            val binder = b as LocalBinder
            service = binder.service
            refreshTasks()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            service = null
        }
    }

    private val localBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                DownloadService.ACTION_TASKS_CHANGED -> {
                    // Refresh the list of downloading
                    // tasks
                    runOnUiThread {
                        refreshTasks()
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup recycler-view
        val adapter = DownloadTaskAdapter(tasks)
        recycler.adapter = adapter

        fab.setOnClickListener {
            if (!permissionsGranted) {
                // Make sure user granted required permissions
                // to our application
                checkRuntimePermissions()
                return@setOnClickListener
            }

            val intent = Intent(this, DownloadService::class.java).apply {
                putExtra(DownloadService.EXTRA_DOWNLOAD_URL, PLACEHOLDER_URL)
            }

            startForegroundService(intent)
        }

        // Make sure user granted required permissions
        // to our application
        checkRuntimePermissions()
    }

    private fun checkRuntimePermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionRationaleShouldBeShown(
                            permissions: MutableList<PermissionRequest>?,
                            token: PermissionToken?) {
                    }

                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        permissionsGranted = report.areAllPermissionsGranted()
                    }
                })
                .check()
    }

    override fun onStart() {
        super.onStart()
        val filter = IntentFilter(DownloadService.ACTION_TASKS_CHANGED)
        val lbm = LocalBroadcastManager.getInstance(this)
        lbm.registerReceiver(localBroadcastReceiver, filter)

        // Bind to download service
        val intent = Intent(this, DownloadService::class.java)
        bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        val lbm = LocalBroadcastManager.getInstance(this)
        lbm.unregisterReceiver(localBroadcastReceiver)

        // Bind from download service
        unbindService(connection)
        service = null
    }

    private fun refreshTasks() {
        tasks.clear()
        service?.also { tasks.addAll(it.tasks) }
        recycler.adapter.notifyDataSetChanged()
    }

}
