package com.example.downloadmanager.ui.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import com.example.downloadmanager.R
import com.example.downloadmanager.services.DownloadService

class DownloadTaskView : RelativeLayout {

    companion object {
        private const val REFRESH_PERIOD = 1000L // ms
    }

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private var task: DownloadService.DownloadTask? = null

    private val handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
            bind()

            // Schedule next update
            sendEmptyMessageDelayed(0, REFRESH_PERIOD)
        }
    }

    private lateinit var titleTextView: TextView
    private lateinit var timeTextView: TextView
    private lateinit var progressView: ProgressBar

    override fun onFinishInflate() {
        super.onFinishInflate()
        titleTextView = findViewById(R.id.title)
        timeTextView = findViewById(R.id.time)
        progressView = findViewById(R.id.progress)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()

        if (task != null) {
            handler.sendEmptyMessage(0)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        handler.removeCallbacksAndMessages(null)
    }

    fun setTask(task: DownloadService.DownloadTask?) {
        this.task = task

        if (task == null) {
            handler.removeCallbacksAndMessages(null)
        } else if (isAttachedToWindow) handler.sendEmptyMessage(0)
    }

    private fun bind() {
        val t = task
        if (t == null) {
            titleTextView.text = null
            progressView.visibility = View.GONE
            timeTextView.visibility = View.GONE
            return
        }

        titleTextView.text = t.url
        progressView.visibility = View.VISIBLE

        if (t.currentBytes == 0L) {
            timeTextView.visibility = View.GONE
            progressView.isIndeterminate = true
        } else {
            val progress = t.currentBytes * 100.0 / t.totalBytes
            val progressCapped = Math.max(Math.min(progress, 100.0), 0.0)
            progressView.isIndeterminate = false
            progressView.progress = progressCapped.toInt()

            val elapsedTime = t.getElapsedTime()
            if (elapsedTime != 0L && progressCapped != 0.0) {
                val remainingTime = (elapsedTime * 100.0 / progressCapped - elapsedTime).toLong()
                timeTextView.visibility = View.VISIBLE
                timeTextView.text = "${remainingTime / 1000}s"
            } else {
                timeTextView.visibility = View.GONE
            }
        }
    }

}