package com.example.downloadmanager.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.Service
import android.content.Intent
import java.net.URL
import java.util.*
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import android.os.*
import android.os.IBinder
import com.example.downloadmanager.R
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.drawable.Icon
import android.support.v4.content.LocalBroadcastManager


class DownloadService : Service() {

    companion object {
        const val EXTRA_DOWNLOAD_URL = "extra::download_url"

        const val ACTION_TASKS_CHANGED = "action_tasks_changed"

        private const val NOTIFICATION_ID = 1
        private const val NOTIFICATION_CHAN_NAME = "Download Service"
        private const val NOTIFICATION_CHAN_ID = "download_service"

        private const val MESSAGE_STOP_MAYBE = 1
        private const val MESSAGE_TASKS_CHANGED = 4
        private const val MESSAGE_MAIN_NOTIFICATION = 2
        private const val MESSAGE_TASK_NOTIFICATION = 3

        /*
         * Gets the number of available cores
         * (not always the same as the maximum number of cores)
         */
        private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()
        // The amount of time an idle thread waits before terminating
        private const val KEEP_ALIVE_TIME = 1L
        private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
    }

    val tasks = LinkedBlockingQueue<DownloadTask>()

    private val handler = MyHandler()
    private val binder = LocalBinder()

    private val pool = ThreadPoolExecutor(
            NUMBER_OF_CORES, NUMBER_OF_CORES,
            KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT,
            LinkedBlockingQueue<Runnable>())

    private lateinit var watcher: Watcher

    override fun onCreate() {
        super.onCreate()
        createAndPostNotificationChannel()

        watcher = Watcher(tasks, handler)
        watcher.start()
    }

    private fun createAndPostNotificationChannel() {
        val importance = NotificationManager.IMPORTANCE_LOW
        val channel = NotificationChannel(NOTIFICATION_CHAN_ID, NOTIFICATION_CHAN_NAME, importance)
        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nm.createNotificationChannel(channel)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.run {
            // Add url to download tasks
            val url = getStringExtra(EXTRA_DOWNLOAD_URL)
            val task = DownloadTask(watcher, url)
            tasks.add(task)
            pool.execute(task)

            sendTasksChangedBroadcast()
        }

        startForeground(NOTIFICATION_ID, createNotification())
        return Service.START_REDELIVER_INTENT
    }

    private fun createNotification(): Notification {
        val count = tasks.filter { it.state == DownloadTask.STATE_PROCESSING }.size
        val icon = Icon.createWithResource(this, R.drawable.ic_download_white_24dp)

        return Notification.Builder(this, NOTIFICATION_CHAN_ID)
                .setContentTitle("Download manager")
                .apply {
                    if (count > 0) {
                        setContentText("$count active downloads")
                    }
                }
                .setSmallIcon(icon)
                .setShowWhen(false)
                .build()
    }

    override fun onDestroy() {
        super.onDestroy()
        watcher.finish()
        handler.removeCallbacksAndMessages(null)

        // Hide permanent notification
        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        nm.cancel(NOTIFICATION_ID)
    }

    override fun onBind(p0: Intent?): IBinder = binder

    private fun sendTasksChangedBroadcast() {
        val intent = Intent(ACTION_TASKS_CHANGED)
        val lbm = LocalBroadcastManager.getInstance(this@DownloadService)
        lbm.sendBroadcast(intent)
    }

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        internal val service: DownloadService
            get() = this@DownloadService
    }

    @SuppressLint("HandlerLeak")
    inner class MyHandler : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)

            when (msg.what) {
                MESSAGE_STOP_MAYBE -> {
                    // Request to stop the service if no
                    // pending tasks available.
                    if (tasks.isEmpty()) stopSelf()
                }
                MESSAGE_TASKS_CHANGED -> sendTasksChangedBroadcast()
                MESSAGE_MAIN_NOTIFICATION -> {
                    val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    nm.notify(NOTIFICATION_ID, createNotification())
                }
                MESSAGE_TASK_NOTIFICATION -> {
                    val task = msg.obj as DownloadTask
                    val icon = Icon.createWithResource(this@DownloadService, R.drawable.ic_download_white_24dp)
                    val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val n = Notification.Builder(this@DownloadService, NOTIFICATION_CHAN_ID)
                            .setContentTitle(if (task.state == DownloadTask.STATE_COMPLETED) {
                                "Download completed"
                            } else "Download failed")
                            .setContentText(task.url)
                            .setSmallIcon(icon)
                            .setWhen(System.currentTimeMillis())
                            .setShowWhen(true)
                            .build()

                    nm.notify(task.url.hashCode() shl 1, n)
                }
            }
        }
    }

    //
    // STATIC CLASSES
    //

    /**
     * Watcher the download queue, requests to send notifications
     * updates.
     */
    class Watcher(private val tasks: Queue<DownloadTask>, private val handler: Handler) : Thread() {

        private val monitor = Object()
        @Volatile
        private var locked = false
        @Volatile
        private var running = true

        override fun run() {
            while (running) {
                var changed = false
                val iterator = tasks.iterator()
                while (iterator.hasNext()) {
                    val task = iterator.next()
                    if (task.state != DownloadTask.STATE_PROCESSING) {

                        // Post new notification about this download task,
                        // successful or not.
                        val msg = Message.obtain(handler, MESSAGE_TASK_NOTIFICATION, task)
                        handler.sendMessage(msg)

                        // Remove current task from the list.
                        changed = true
                        iterator.remove()
                    }
                }

                if (tasks.size == 0) {
                    handler.sendEmptyMessage(MESSAGE_STOP_MAYBE)
                }

                if (changed) {
                    handler.sendEmptyMessage(MESSAGE_TASKS_CHANGED)
                    handler.sendEmptyMessage(MESSAGE_MAIN_NOTIFICATION)
                }

                synchronized(monitor, {
                    try {
                        locked = true
                        monitor.wait(2000)
                    } catch (e: InterruptedException) {
                    } finally {
                        locked = false
                    }
                })
            }
        }

        fun finish() {
            running = false
            ping()
        }

        fun ping() {
            synchronized(monitor, {
                if (locked) {
                    monitor.notifyAll()
                }
            })
        }

    }

    class DownloadTask(private val watcher: Watcher, val url: String) : Runnable {
        /**
         * Can be one of the following
         * [STATE_COMPLETED], [STATE_FAILED], [STATE_PROCESSING].
         */
        @Volatile
        var state: Int = STATE_PROCESSING
        @Volatile
        var currentBytes: Long = 0
        @Volatile
        var totalBytes: Long = 0
        /**
         * Time when task started to
         * download url. See `SystemClock.elapsedRealtime()`
         */
        @Volatile
        var timeStarted: Long = 0

        companion object {
            private const val TAG = "DownloadTask"

            const val STATE_PROCESSING = 0
            const val STATE_COMPLETED = 1
            const val STATE_FAILED = 2
        }

        override fun run() {
            var inputStream: InputStream? = null
            var outputStream: FileOutputStream? = null
            try {
                val url = URL(url)
                val connection = url.openConnection()
                connection.connect()

                timeStarted = SystemClock.elapsedRealtime()
                totalBytes = connection.contentLengthLong
                Log.v(TAG, "Length of the file[$url]: $totalBytes")

                inputStream = url.openStream()

                val dir = File(Environment.getExternalStorageDirectory(), "downloader/")
                if (!dir.exists()) {
                    dir.mkdirs()
                }

                val filename = UUID.randomUUID().toString()
                outputStream = FileOutputStream("$dir/$filename")

                val data = ByteArray(1024)
                while (true) {
                    val count = inputStream.read(data)
                    if (count == -1) {
                        break // downloading finished
                    }

                    currentBytes += count
                    outputStream.write(data, 0, count)
                }

                // Mark this task as completed
                Log.v(TAG, "Downloading file[$url]/[$filename] completed")
                state = DownloadTask.STATE_COMPLETED
            } catch (e: Exception) {
                // Mark this task as failed
                Log.v(TAG, "Downloading file[$url] failed")
                state = DownloadTask.STATE_FAILED
                e.printStackTrace()
            } finally {
                inputStream?.close()
                outputStream?.close()
            }

            watcher.ping()
        }

        fun getElapsedTime(): Long = if (timeStarted != 0L) {
            SystemClock.elapsedRealtime() - timeStarted
        } else 0
    }

}